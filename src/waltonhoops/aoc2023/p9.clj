(ns waltonhoops.aoc2023.p9
  (:require
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils]))

(defn parse-input [input]
  (->> input
       (map #(re-seq #"\-?\d+" %))
       (map #(mapv parse-long %))))

(def sample-data
  (->> "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45"
       string/split-lines
       parse-input))

(def data
  (->> "p9"
       utils/file-lines
       parse-input))

(defn next-reading [readings]
  (loop [readings readings
         acc '()]
    (if (every? zero? readings)
      (reduce (fn [diff lst]
                (+ diff lst)) 0 acc)
      (recur (->> readings
                  (partition 2 1)
                  (map #(- (second %) (first %))))
             (conj acc (last readings))))))

(defn do-p1 [input]
  (->> input
       (map next-reading)
       (reduce +)))

(defn do-p2 [input]
  (->> input
       (map reverse)
       (map next-reading)
       (reduce +)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
