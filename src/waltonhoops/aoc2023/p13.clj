(ns waltonhoops.aoc2023.p13
  (:require
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils :refer [grid-point]]))

(defn create-grid [input]
  (->> input
       (mapv #(mapv str %))))

(def sample-data
  (->> "#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
"
       string/split-lines
       (partition-by #(= "" %))
       (remove #(= '("") %))
       (map create-grid)))

(def data
  (->> "p13"
       utils/file-lines
       (partition-by #(= "" %))
       (remove #(= '("") %))
       (map create-grid)))

(defn is-hortizontal-reflection? [grid pos]
  (loop [r1 (dec pos)
         r2 pos]
    (cond
      (or (= -1 r1) (= (count grid) r2)) true
      (not= (nth grid r1) (nth grid r2)) false
      :else (recur (dec r1) (inc r2)))))

(defn find-horizontal-reflection [grid]
  (let [positions (range 1 (count grid))]
    (->> positions
         (filter #(is-hortizontal-reflection? grid %))
         first)))

(defn get-column [grid col]
  (reduce #(conj %1 (grid-point grid [col %2])) [] (range (count grid))))

(defn is-vertical-reflection? [grid pos]
  (loop [r1 (dec pos)
         r2 pos]
    (cond
      (or (= -1 r1) (= (count (first grid)) r2)) true
      (not= (get-column grid r1) (get-column grid r2)) false
      :else (recur (dec r1) (inc r2)))))

(defn find-vertical-reflection [grid]
  (let [positions (range 1 (count (first grid)))]
    (->> positions
         (filter #(is-vertical-reflection? grid %))
         first)))

(defn score-grid [grid]
  (if-let [r (find-horizontal-reflection grid)]
    (* 100 r)
    (find-vertical-reflection grid)))

(defn do-p1 [input]
  (->> input
       (map score-grid)
       (reduce +)))

(defn count-differences [v1 v2]
  (->> v1
       (map-indexed #(if (= %2 (nth v2 %1)) 0 1))
       (reduce +)))

(defn is-smudged-hortizontal-reflection? [grid pos]
  (loop [r1 (dec pos)
         r2 pos
         diff 0]
    (let [row1 (nth grid r1)
          row2 (nth grid r2)
          diff (+ diff (count-differences row1 row2))]
      (cond
        (> diff 1) false

        (and
         (or (zero? r1) (= (dec (count grid)) r2))
         (= diff 1)) true

        (or (zero? r1) (= (dec (count grid)) r2)) false

        :else (recur (dec r1) (inc r2) diff)))))

(defn find-smudged-horizontal-reflection [grid]
  (let [positions (range 1 (count grid))]
    (->> positions
         (filter #(is-smudged-hortizontal-reflection? grid %))
         first)))

(defn is-smudged-vertical-reflection? [grid pos]
  (loop [r1 (dec pos)
         r2 pos
         diff 0]
    (let [col1 (get-column grid r1)
          col2 (get-column grid r2)
          diff (+ diff (count-differences col1 col2))]
      (cond
        (> diff 1) false

        (and
         (or (zero? r1) (= (dec (count (first grid))) r2))
         (= diff 1)) true

        (or (zero? r1) (= (dec (count (first grid))) r2)) false

        :else (recur (dec r1) (inc r2) diff)))))

(defn find-smudged-vertical-reflection [grid]
  (let [positions (range 1 (count (first grid)))]
    (->> positions
         (filter #(is-smudged-vertical-reflection? grid %))
         first)))

(defn score-smudged-grid [grid]
  (if-let [r (find-smudged-horizontal-reflection grid)]
    (* 100 r)
    (find-smudged-vertical-reflection grid)))

(defn do-p2 [input]
  (->> input
       (map score-smudged-grid)
       (reduce +)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
