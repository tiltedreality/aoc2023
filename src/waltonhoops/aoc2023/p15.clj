(ns waltonhoops.aoc2023.p15
  (:require
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils]))

(def sample-data
  (-> "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"
      (string/split #",")))

(def data
  (->> "p15"
       utils/file-lines
       first
       (#(string/split % #","))))

(defn- hash-internal [acc c]
  (-> acc
      (+ (int c))
      (* 17)
      (rem 256)))

(defn hash-str [s]
  (reduce hash-internal 0 s))

(defn do-p1 [input]
  (->> input
       (map hash-str)
       (reduce +)))

(defn parse-instruction [instruction]
  (let [[_ label rst] (re-find #"(\w+)(.+)" instruction)
        action (if (string/ends-with? rst "-") "-" (->> rst last str parse-long))]
    [label action]))

(defn create-boxes []
  (vec (repeat 256 '())))

(defn update-box [content label action]
  (cond
    (= "-" action) (remove #(= label (first %)) content)

    (some #(= label (first %)) content) (map #(if (= label (first %)) [label action] %) content)

    :else (cons [label action] content)))

(defn update-boxes [boxes instruction]
  (let [[label action] (parse-instruction instruction)
        box-index (hash-str label)]
    (update boxes box-index update-box label action)))

(defn lens-power [box-index lens-index [_ focus]]
  (* (inc box-index)
     (inc lens-index)
     focus))

(defn box-power [box-index contents]
  (->> contents
       (map-indexed #(lens-power box-index %1 %2))
       (reduce +)))

(defn do-p2 [input]
  (let [boxes (create-boxes)]
    (->> input
         (reduce update-boxes boxes)
         (map reverse)
         (map-indexed box-power)
         (reduce +))))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
