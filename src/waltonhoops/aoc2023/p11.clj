(ns waltonhoops.aoc2023.p11
  (:require
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils :refer [grid-point]]))

(defn create-grid [input]
  (->> input
       (mapv #(mapv str %))))

(def sample-data
  (->>
   "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#....."
   string/split-lines
   create-grid))

(def data
  (->> "p11"
       utils/file-lines
       create-grid))

(defn print-universe [universe]
  (->> universe
       (map string/join)
       (map println)
       dorun))

(defn empty-row? [row]
  (every? #(= "." %) row))

(defn expand-rows [universe]
  (vec
   (mapcat (fn [row]
             (if (empty-row? row)
               [row row]
               [row])) universe)))

(defn find-empty-columns [universe]
  (let [row-count (count universe)]
    (reduce (fn [empty-cols col-idx]
              (let [col (for [row (range row-count)]
                          (grid-point universe [col-idx row]))]
                (if (every? #(= "." %) col)
                  (conj empty-cols col-idx)
                  empty-cols)))
            #{}
            (range (count (first universe))))))

(defn expand-row-columns [row cols]
  (->> row
       (map-indexed (fn [idx val]
                      (if (cols idx)
                        [val val]
                        [val])))
       (apply concat)
       vec))

(defn expand-empty-cols [universe empty-cols]
  (vec (map #(expand-row-columns % empty-cols) universe)))

(defn expand-colums [universe]
  (let [empties (find-empty-columns universe)]
    (expand-empty-cols universe empties)))

(defn expand-universe [universe]
  (->> universe
       expand-rows
       expand-colums))

(defn find-galaxies-row [row]
  (->> row
       (map-indexed #(when (= "#" %2) %1))
       (remove nil?)))

(defn find-galaxies [universe]
  (->> universe
       (map-indexed (fn [y row]
                      (for [x (find-galaxies-row row)]
                        [x y])))
       (apply concat)))

(defn taxicab-distance [[x1 y1] [x2 y2]]
  (+
   (abs (- x1 x2))
   (abs (- y1 y2))))

(defn do-p1 [input]
  (let [universe (expand-universe input)
        galaxies (find-galaxies universe)
        pairs (for [n1 (range (count galaxies))
                    n2 (range (inc n1) (count galaxies))]
                [(nth galaxies n1) (nth galaxies n2)])]
    (->> pairs
         (map #(apply taxicab-distance %1))
         (reduce +))))

(defn find-empty-rows [universe]
  (->> universe
       (map-indexed (fn [idx row]
                      (when (empty-row? row)
                        idx)))
       (remove nil?)
       (into #{})))

(def million 1000000)

(defn apply-expansion [[x y] empty-rows empty-cols]
  (let [col-expansion (count (filter #(< % x) empty-cols))
        row-expansion (count (filter #(< % y) empty-rows))]
    [(+ x (- (* col-expansion million) col-expansion)) (+ y (- (* row-expansion million) row-expansion))]))

(defn do-p2 [input]
  (let [galaxies (find-galaxies input)
        empty-rows (find-empty-rows input)
        empty-cols (find-empty-columns input)
        galaxies (map #(apply-expansion % empty-rows empty-cols) galaxies)
        pairs (for [n1 (range (count galaxies))
                    n2 (range (inc n1) (count galaxies))]
                [(nth galaxies n1) (nth galaxies n2)])]
    (->> pairs
         (map #(apply taxicab-distance %1))
         (reduce +))))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
