(ns waltonhoops.aoc2023.p6
  (:require [waltonhoops.aoc2023.utils :as utils]
            [clojure.string :as string]))

(def sample-data
  (string/split-lines
   "Time:      7  15   30
Distance:  9  40  200"))

(def data
  (utils/file-lines "p6"))

(defn parse-input [input]
  (map vector
       (map parse-long )
       (map parse-long (re-seq #"\d+" (second input)))))

(defn score-option [button-hold race-time]
  (let [time-remaining (- race-time button-hold)]
    (* button-hold time-remaining)))

(defn winning-moves [[time distance]]
  (->> (range 1 time)
       (map #(score-option %1 time))
       (filter #(> %1 distance))))

(defn do-p1 [data]
  (->> data
       parse-input
       (map winning-moves)
       (map count)
       (reduce *)))

(defn parse-input-p2 [input]
  [(->> input first (re-seq #"\d+") (apply str) parse-long)
   (->> input second (re-seq #"\d+") (apply str) parse-long)])

(defn do-p2 [input]
  (->> input
       parse-input-p2
       winning-moves
       count))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
