(ns waltonhoops.aoc2023.p16
  (:require
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils :refer [create-grid grid-dims
                                                grid-point in-grid?]]))

(def sample-data
  (->>
   ".|...\\....
|.-.\\.....
.....|-...
........|.
..........
.........\\
..../.\\\\..
.-.-/..|..
.|....-|.\\
..//.|...."
   string/split-lines
   create-grid))

(def data
  (->> "p16"
       utils/file-lines
       create-grid))

(defmulti adjust-beam
  (fn [contraption delta] contraption))

(defmethod adjust-beam "." [_ point-delta]
  [point-delta])

(defmethod adjust-beam "\\" [_ [point [delta-x delta-y]]]
  [[point [delta-y delta-x]]])

(defmethod adjust-beam "/" [_ [point [delta-x delta-y]]]
  [[point [(* -1 delta-y) (* -1 delta-x)]]])

(defmethod adjust-beam "-" [_ [point [delta-x delta-y]]]
  (if (zero? delta-y)
    [[point [delta-x delta-y]]]
    [[point [-1 0]] [point [1 0]]]))

(defmethod adjust-beam "|" [_ [point [delta-x delta-y]]]
  (if (zero? delta-x)
    [[point [delta-x delta-y]]]
    [[point [0 -1]] [point [0 1]]]))

(defn apply-delta [[[x y] [delta-x delta-y]]]
  [[(+ x delta-x) (+ y delta-y)] [delta-x delta-y]])

(defn delta-point-in-grid [grid [point _delta]]
  (in-grid? grid point))

(defn trace-beam [grid point-delta]
  (loop [visited #{}
         beams [point-delta]]
    (let [[beam & beams] beams]
      (cond
        (nil? beam) (->> visited (map first) (into #{}))

        (visited beam) (recur visited beams)

        :else (recur (conj visited beam)
                     (lazy-cat
                      (->> beam
                           (adjust-beam (grid-point grid (first beam)))
                           (map apply-delta)
                           (filter #(delta-point-in-grid grid %)))
                      beams))))))

(defn do-p1 [input]
  (count (trace-beam input [[0 0] [1 0]])))

(defn possible-starts [grid]
  (let [[x-dim y-dim] (grid-dims grid)]
    (lazy-cat
     ;; enter from the top
     (for [x (range x-dim)]
       [[x 0] [0 1]])
     ;; enter from the left
     (for [y (range y-dim)]
       [[0 y] [1 0]])
     ;; enter from the bottom
     (for [x (range x-dim)]
       [[x (dec y-dim)] [0 -1]])
     ;; enter from the right
     (for [y (range y-dim)]
       [[(dec x-dim) y] [-1 0]]))))

(defn do-p2 [input]
  (->> input
       possible-starts
       (pmap #(trace-beam input %))
       (map count)
       (apply max)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
