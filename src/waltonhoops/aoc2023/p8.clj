(ns waltonhoops.aoc2023.p8
  (:require [clojure.string :as string]
            [waltonhoops.aoc2023.utils :as utils]
            [clojure.math.numeric-tower :refer [lcm]]))

(def sample-data-short
  (string/split-lines
   "RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)"))

(def sample-data-long
  (string/split-lines
   "LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)"))

(def data (utils/file-lines "p8"))

(defn parse-node [node-str]
  (let [[node left right] (->> node-str (re-seq #"\w+") vec)]
    {node [left right]}))

(defn parse-input [input]
  (let [[instruction-line & nodes] input]
    {:instructions (map str instruction-line)
     :nodes (->> nodes
                 (remove #(= "" %))
                 (map parse-node)
                 (into {}))}))

(defn next-node [node instruction nodes]
  (if (= "L" instruction)
    (-> node nodes first)
    (-> node nodes second)))

(defn do-p1 [input]
  (let [{:keys [instructions nodes]} (parse-input input)]
    (loop [[instruction & instructions] (cycle instructions)
           node "AAA"
           cnt 0]
      (if (= node "ZZZ")
        cnt
        (recur instructions
               (next-node node instruction nodes)
               (inc cnt))))))

(def p2-sample
  (string/split-lines
   "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)"))

(defn starting-nodes [nodes]
  (->> nodes
       keys
       (filter #(= \A (last %)))))

(defn is-ending-node? [node]
  (= \Z (last node)))

(defn cycle-length [starting-node instructions nodes]
  (loop [[instruction & instructions] instructions
         node starting-node
         cnt 0]
    (if (is-ending-node? node)
      cnt
      (recur instructions
             (next-node node instruction nodes)
             (inc cnt)))))

(defn do-p2 [input]
  (let [{:keys [instructions nodes]} (parse-input input)
        instructions (cycle instructions)]
    (->> nodes
         starting-nodes
         (map #(cycle-length %1 instructions nodes))
         (reduce lcm))))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
