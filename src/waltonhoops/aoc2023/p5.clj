(ns waltonhoops.aoc2023.p5
  (:require [clojure.string :as string]
            [waltonhoops.aoc2023.utils :as utils]))

(defn parse-map-line [line]
  (let [[dest source range] (->> line (re-seq #"\d+") (map parse-long))]
    {:dest dest
     :source source
     :range range}))

(defn parse-map [map-strs]
  (let [[name-line & rst] map-strs
        name (->> name-line (re-seq #"[\w|-]+") first keyword)]
    {name (map parse-map-line rst)}))

(defn parse-input [input]
  (let [[seed-line & rst] input
        seeds (->> seed-line (re-seq #"\d+") (map parse-long))]
    (into {} [{:seeds seeds}
              (->> rst
                   (partition-by #(= "" %))
                   (remove #(= '("") %))
                   (map parse-map)
                   (into {}))])))

(def sample-data
  (parse-input
   (string/split-lines
    "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4")))

(def data
  (->> "p5"
       (utils/file-lines)
       (parse-input)))

(defn entry-check [val {:keys [dest source range]}]
  (let [offset (- val source)]
    (when (<= 0 offset (dec range))
      (+ dest offset))))

(defn map-lookup [val map]
  (if-let [result (some #(entry-check val %) map)]
    result
    val))

(defn seed->location [seed maps]
  (-> seed
      (map-lookup (:seed-to-soil maps))
      (map-lookup (:soil-to-fertilizer maps))
      (map-lookup (:fertilizer-to-water maps))
      (map-lookup (:water-to-light maps))
      (map-lookup (:light-to-temperature maps))
      (map-lookup (:temperature-to-humidity maps))
      (map-lookup (:humidity-to-location maps))))

(defn seed-locations [data]
  (map #(seed->location % data) (:seeds data)))

(defn do-p1 [data]
  (->> data
       seed-locations
       (apply min)))

(defn calculate-range [[start end]]
  [start (inc (- end start))])

(defn invalid-range? [[_ range]]
  (< range 1))

(defn process-seeds-entry [[start seed-range] {:keys [dest source range]}]
  (let [seed-end (+ start (dec seed-range))
        range-end (+ source (dec range))
        affected-start (max start source)
        affected-end (min seed-end range-end)
        result-start (+ dest (- affected-start source))
        result-end (+ dest (- affected-end source))
        result-range (calculate-range [result-start result-end])
        bounding [[start (min seed-end (dec affected-start))]
                  [(max (inc affected-end) start) seed-end]]]
    {:affected (when (not (invalid-range? result-range))
                 result-range)
     :unaffected (->> bounding
                      (map calculate-range)
                      (remove invalid-range?))}))

(defn process-seed-range-entries [seed-range entries]
  (loop [seeds [seed-range]
         static-seeds []
         [entry & entries] entries]
    (if entry
      (do
        (let [results (map #(process-seeds-entry %1 entry) seeds)]
          (recur
           (->> results (map :unaffected) (apply concat))
           (concat static-seeds (->> results (map :affected) (remove nil?)))
           entries)))
      (concat seeds static-seeds))))

(defn process-seeds-map [seed-ranges map-entries]
  (mapcat #(process-seed-range-entries %1 map-entries) seed-ranges))

(defn process-seed-range [range maps]
  (-> [range]
      (process-seeds-map (:seed-to-soil maps))
      (process-seeds-map (:soil-to-fertilizer maps))
      (process-seeds-map (:fertilizer-to-water maps))
      (process-seeds-map (:water-to-light maps))
      (process-seeds-map (:light-to-temperature maps))
      (process-seeds-map (:temperature-to-humidity maps))
      (process-seeds-map (:humidity-to-location maps))))

(defn do-p2 [data]
  (let [ranges (->> data :seeds (partition 2))]
    (->> ranges
         (mapcat #(process-seed-range %1 data))
         (map first)
         (apply min))))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
