(ns waltonhoops.aoc2023.p3
  (:require [waltonhoops.aoc2023.utils :as utils]
            [clojure.string :as string]
            [clojure.set :as set]))

(def sample-data
  (string/split-lines "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."))

(def data
  (utils/file-lines "p3"))

(defn extract-result [result s]
  [(.start result) (subs s (.start result) (.end result))])


(defn matches [re s]
  (->> s
       (re-matcher re)
       (.results)
       (.toList)
       (map #(extract-result % s))))

(defn extract-index [row results]
  (map (fn [[col s]] [[row col] s]) results))

(defn build-index [input re]
  (->> input
       (map #(matches re %))
       (map-indexed extract-index)
       (apply concat)
       (into {})))

(defn parse-input [input]
  (let [parts-index (build-index input #"\d+")
        indicator-index (build-index input #"[^\.\d]")]
    [parts-index indicator-index]))

(defn surronding-indexes [row col s]
  (let [cols (range (dec col) (inc (+ col (count s))))]
    (concat
     ; points above our string
     (map (fn [col] [(dec row) col]) cols)
     ; points around our string
     [[row (dec col)] [row (+ col (count s))]]
     ; points below our string
     (map (fn [col] [(inc row) col]) cols))))

(defn valid-part? [[[row col] s] indicator-index]
  (let [surronding (surronding-indexes row col s)]
    (boolean (some indicator-index surronding))))

(defn do-p1 [input]
  (let [[parts-index indicator-index] (parse-input input)]
    (->> parts-index
         (filter #(valid-part? % indicator-index))
         (map second)
         (map parse-long)
         (reduce +))))

(defn is-gear? [[[_row _col] s]]
  (= s "*"))

(defn adjacent-gears [[[row col] s] gear-index]
  (let [surronding (into #{} (surronding-indexes row col s))
        gears (set/intersection surronding gear-index)]
    (map (fn [g] [s g]) gears)))

(defn gear-ratio [[_g [[n1 _] [n2 _]]]]
  (* (parse-long n1) (parse-long n2)))

(defn do-p2 [input]
  (let [[parts-index indicator-index] (parse-input input)
        gear-index (->> indicator-index
                        (filter is-gear?)
                        (map first)
                        (into #{}))]
    (->> parts-index
         (mapcat #(adjacent-gears % gear-index))
         (group-by second)
         (filter #(= 2 (-> % second count)))
         (map gear-ratio)
         (reduce +))))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
