(ns waltonhoops.aoc2023.p2
  (:require
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils]))


(def sample-data
  (string/split-lines
   "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"))

(def data (utils/file-lines "p2"))

(defn parse-pair [[cnt color]]
  [(keyword color) (parse-long cnt)])

(defn parse-round [round]
  (->> round
       (map #(string/split % #"\s"))
       (map parse-pair)
       (into {})))

(defn parse-game-desc [desc]
  (let [[_ num] (string/split desc #"\s")]
    (parse-long num)))

(defn parse-game-str [game-str]
  (let [[game-desc rounds] (string/split game-str #"\s*:\s*")
        rounds (string/split rounds #"\s*;\s*")
        rounds (map #(string/split % #"\s*,\s*") rounds)
        rounds (map parse-round rounds)]
    [(parse-game-desc game-desc) rounds]))

(defn parse-input [input]
  (->> input
       (map parse-game-str)))

(def bag-contents {:red 12
                   :green 13
                   :blue 14})

(defn color-possible? [[color cnt] contents]
  (<= cnt (color contents)))

(defn round-possible? [round contents]
  (every? #(color-possible? % contents) round))

(defn game-possible? [[_ rounds] contents]
  (every? #(round-possible? % contents) rounds))

(defn do-p1 [input]
  (->> input
       parse-input
       (filter #(game-possible? % bag-contents))
       (map first)
       (reduce +)))

(defn min-required [[_game rounds]]
  (->> rounds
       (cons {:red 0 :blue 0 :green 0})
       (apply merge-with max)))

(defn game-power [required]
  (reduce * (vals required)))

(defn do-p2 [input]
  (->> input
       parse-input
       (map min-required)
       (map game-power)
       (reduce +)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
