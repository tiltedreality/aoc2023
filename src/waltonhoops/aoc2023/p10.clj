(ns waltonhoops.aoc2023.p10
  (:require
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils :refer [grid-point in-grid?]]))

(defn create-grid [input]
  (->> input
       (mapv #(mapv str %))))

(def sample-data-simple
  (->> "-L|F7
7S-7|
L|7||
-L-J|
L|-JF"
       string/split-lines
       create-grid))

(def sample-data-complex
  (->> "..F7.
.FJ|.
SJ.L7
|F--J
LJ..."
       string/split-lines
       create-grid))

(def data
  (->> "p10"
       utils/file-lines
       create-grid))

(defn find-start-row [y row]
  (->> row
       (keep-indexed (fn [x val]
                       (when (= val "S")
                         [x y])))
       first))

(defn find-start [grid]
  (->> grid
       (keep-indexed find-start-row)
       first))

(defn connects-to [grid [x y]]
  (case (grid-point grid [x y])
    "." []
    "|" [[x (dec y)]
         [x (inc y)]]
    "-" [[(dec x) y]
         [(inc x) y]]
    "L" [[x (dec y)]
         [(inc x) y]]
    "J" [[x (dec y)]
         [(dec x) y]]
    "7" [[x (inc y)]
         [(dec x) y]]
    "F" [[x (inc y)]
         [(inc x) y]]
    "S" []))

(defn around-point [grid [x y]]
  (filter #(in-grid? grid %) [[(inc x) y] [(dec x) y] [x (inc y)] [x (dec y)]]))

(defn in-list? [lst value]
  (boolean (some #(= value %) lst)))

(defn find-seed [grid start-point]
  (->> start-point
       (around-point grid)
       (filter #(in-list? (connects-to grid %) start-point))
       first))

(defn next-point [grid prev point]
  (->> point
       (connects-to grid)
       (filter #(not= prev %))
       first))

(defn loop-size [grid]
  (let [start (find-start grid)
        seed (find-seed grid start)]
    (loop [point seed
           prev start
           size 1]
      (if (= start point)
        size
        (recur (next-point grid prev point)
               point
               (inc size))))))

(defn do-p1 [input]
  (-> input
      loop-size
      (/ 2)))

(def p2-sample-1
  (->
   "...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
..........."
   string/split-lines
   create-grid))

(def p2-sample-2
  (->
   "..........
.S------7.
.|F----7|.
.||OOOO||.
.||OOOO||.
.|L-7F-J|.
.|II||II|.
.L--JL--J.
.........."
   string/split-lines
   create-grid))

(def p2-sample-3
  (->
   "FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L"
   string/split-lines
   create-grid))

(defn full-loop [grid]
  (let [start (find-start grid)
        seed (find-seed grid start)]
    (loop [point seed
           prev start
           points [start]]
      (if (= start point)
        (conj points point)
        (recur (next-point grid prev point)
               point
               (conj points point))))))

;; shoelace formula
(defn shoelace [path]
  (let [[p1 & path] path]
    (loop [p1 p1
           [p2 & path] path
           acc 0]
      (if (not p2)
        (/ (abs acc) 2)
        (let [[x1 y1] p1
              [x2 y2] p2]
          (recur
           p2
           path
           (+ acc
              (- (* y2 x1)
                 (* y1 x2)))))))))

;; picks theorem
(defn picks [area cnt]
  (- (+ area 1) (/ cnt 2)))

(defn do-p2 [input]
  (let [points (full-loop input)
        area (shoelace points)]
    (picks area (dec (count points)))))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
