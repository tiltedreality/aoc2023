(ns waltonhoops.aoc2023.p7
  (:require [clojure.string :as string]
            [waltonhoops.aoc2023.utils :as utils]))

(def sample-data
  (string/split-lines
   "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"))

(def data
  (utils/file-lines "p7"))

(def card-values {"T" 10
                  "J" 11
                  "Q" 12
                  "K" 13
                  "A" 14})

(defn card->score [card]
  (if-let [val (card-values card)]
    val
    (parse-long card)))

(defn parse-hand [hand]
  (let [[hand-str bid] (string/split hand #" ")]
    {:bid (parse-long bid)
     :cards (->> hand-str
                (map str)
                (map card->score)
                vec)}))

(defn parse-input [input]
  (map parse-hand input))

(def count->strength
  {[5] 6 ; Five of a kind
   [4 1] 5 ; Four of a kind
   [3 2] 4 ; Full House
   [3 1] 3 ; Three of a kind
   [2 2] 2 ; Two pair
   [2 1] 1 ; One pair
   [1 1] 0}) ; High card

(defn cards->strength [cards]
  (->> cards
       (group-by identity)
       vals
       (map count)
       sort
       reverse
       (take 2)
       vec
       count->strength))

(defn hand->strength [{:keys [cards] :as hand}]
  (assoc hand :strength (cards->strength cards)))

(defn do-p1 [input]
  (->> input
       parse-input
       (map hand->strength)
       (sort-by :cards)
       (sort-by :strength)
       (map-indexed #(* (inc %1) (:bid %2)))
       (reduce +)))

(def card-values-2 {"T" 10
                  "J" 1
                  "Q" 12
                  "K" 13
                  "A" 14})

(defn card->score-2 [card]
  (if-let [val (card-values-2 card)]
    val
    (parse-long card)))

(defn parse-hand-2 [hand]
  (let [[hand-str bid] (string/split hand #" ")]
    {:bid (parse-long bid)
     :cards (->> hand-str
                (map str)
                (map card->score-2)
                vec)}))

(defn parse-input-2 [input]
  (map parse-hand-2 input))

(defn apply-jokers [card-map]
  (let [jokers (card-map 1)
        card-map (dissoc card-map 1)
        most-card (->> card-map
                       (sort-by #(->> % second count))
                       last
                       first)]
    (update card-map most-card concat (repeat (count jokers) most-card))))

(defn cards->strength-2 [cards]
  (->> cards
       (group-by identity)
       apply-jokers
       vals
       (map count)
       sort
       reverse
       (take 2)
       vec
       count->strength))

(defn hand->strength-2 [{:keys [cards] :as hand}]
  (assoc hand :strength (cards->strength-2 cards)))


(defn do-p2 [input]
  (->> input
       parse-input-2
       (map hand->strength-2)
       (sort-by :cards)
       (sort-by :strength)
       (map-indexed #(* (inc %1) (:bid %2)))
       (reduce +)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
