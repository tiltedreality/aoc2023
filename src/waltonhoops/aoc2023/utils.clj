(ns waltonhoops.aoc2023.utils
  (:require
   [clojure.java.io :as io]
   [clojure.string :as string]))

(defn file-lines [filename]
  (let [file-path (str "files/" filename)]
    (with-open [read (io/reader file-path)]
      (doall (line-seq read)))))

(defn grid-point [grid [x y]]
  (-> grid
      (nth y)
      (nth x)))

(defn grid-dims [grid]
  [(count (first grid)) (count grid)])

(defn in-grid? [grid [x y]]
  (let [[rows cols] (grid-dims grid)]
    (and (>= x 0)
         (< x rows)
         (>= y 0)
         (< y cols))))

(defn create-grid [input]
  (->> input
       (mapv #(mapv str %))))

(defn print-grid [universe]
  (->> universe
       (map string/join)
       (map println)
       dorun))
