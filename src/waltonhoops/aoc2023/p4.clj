(ns waltonhoops.aoc2023.p4
  (:require
   [clojure.math :refer [pow]]
   [clojure.set :as set]
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils]))

(def sample-data
  (string/split-lines
   "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"))

(def data (utils/file-lines "p4"))

(defn parse-card [line-str]
  (let [[card-str rst] (string/split line-str #":")
        [winning-str ours-str] (string/split rst #"\|")
        card-num (->> card-str
                      (re-find #"\d+")
                      parse-long)
        winning-nums (->> winning-str
                          (re-seq #"\d+")
                          (map parse-long)
                          (into #{}))
        our-nums (->> ours-str
                      (re-seq #"\d+")
                      (map parse-long)
                      (into #{}))]
    {:card card-num
     :winning winning-nums
     :ours our-nums}))

(defn parse-input [input]
  (map parse-card input))

(defn score-card [{:keys [winning ours]}]
  (let [scoring (set/intersection winning ours)]
    (if (seq scoring)
      (->> scoring
           count
           dec
           (pow 2)
           long)
      0)))

(defn do-p1 [input]
  (->> input
       (map parse-card)
       (map score-card)
       (reduce +)))

(defn simple-score-card [{:keys [winning ours]}]
  (let [scoring (set/intersection winning ours)]
    (count scoring)))

(defn add-count [counts idx amt]
  (if (< idx (count counts))
    (update counts idx + amt)
    counts))

(defn update-counts [counts [idx score]]
  (let [amt (nth counts idx)
        update-count #(add-count %1 %2 amt)]
    (reduce update-count counts (range (inc idx) (+ idx score 1)))))

(defn do-p2 [input]
  (let [scores (->> input
                    (map parse-card)
                    (map simple-score-card)
                    (map-indexed vector))
        counts (reduce
                update-counts
                (vec (repeat (count scores) 1))
                scores)]
    (reduce + counts)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
