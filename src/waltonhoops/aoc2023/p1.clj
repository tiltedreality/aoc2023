(ns waltonhoops.aoc2023.p1
  (:require
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils]))

(def sample-data
  (string/split-lines "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"))

(def sample-data2
  (string/split-lines
   "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"))


(def data
  (utils/file-lines "p1"))

(defn first-last [lst]
  [(first lst) (last lst)])

(defn parse-calibration [calibration]
  (->> calibration
       (map str)
       (map parse-long)
       (remove nil?)
       (first-last)
       (apply str)
       (parse-long)))

(def nums {"one" 1
           "two" 2
           "three" 3
           "four" 4
           "five" 5
           "six" 6
           "seven" 7
           "eight" 8
           "nine" 9})

(defn parse-numish [s]
  (or
   (parse-long s)
   (nums s)))

(defn parse-stupid-calibration [calibration]
  (->> calibration
       (re-seq  #"(?=(\d|one|two|three|four|five|six|seven|eight|nine))")
       (map last)
       (first-last)
       (map parse-numish)
       (apply str)
       (parse-long)))

(defn do-p1 [calibrations]
  (->> calibrations
       (map parse-calibration)
       (reduce +)))

(defn do-p2 [calibrations]
  (->> calibrations
       (map parse-stupid-calibration)
       (reduce +)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
