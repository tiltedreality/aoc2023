(ns waltonhoops.aoc2023.p14
  (:require
   [clojure.set :as set]
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils]))

(defn accumulate-rocks [{:keys [round cube max-x max-y]} [y row]]
  {:round (reduce (fn [rounds x]
                    (if (= \O (nth row x))
                      (conj rounds [x y])
                      rounds))
                  round
                  (range (count row)))
   :cube (reduce (fn [cubes x]
                   (if (= \# (nth row x))
                     (conj cubes [x y])
                     cubes))
                 cube
                 (range (count row)))
   :max-x max-x
   :max-y max-y})

(defn parse-input [input]
  (let [max-x (->> input first count dec)
        max-y (->> input count dec)]
    (->> input
         (map-indexed vector)
         (reduce accumulate-rocks {:round #{} :cube #{} :max-x max-x :max-y max-y}))))

(def sample-data
  (->>
   "O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#...."
   string/split-lines
   parse-input))

(def data
  (->> "p14"
       utils/file-lines
       parse-input))

(defn draw [stones]
  (doseq [y (range (inc (:max-y stones)))]
    (->> (range (inc (:max-x stones)))
         (map (fn [x]
                (cond
                  ((:round stones) [x y]) "O"
                  ((:cube stones) [x y]) "#"
                  :else ".")))
         string/join
         println)))

(defn slide-stone [update-fn pos {:keys [round cube max-x max-y]}]
  (let [stones (set/union round cube)]
    (loop [pos pos
           [x y] (update-fn pos)]
      (cond
        (stones [x y]) pos

        (or (< x 0) (> x max-x) (< y 0) (> y max-y)) pos

        :else (recur
               [x y]
               (update-fn [x y]))))))

(def directions
  {:north [(fn [stones] (sort-by second stones)) (fn [stone] (update stone 1 dec))]
   :south [(fn [stones] (reverse (sort-by second stones))) (fn [stone] (update stone 1 inc))]
   :west [(fn [stones] (sort-by first stones)) (fn [stone] (update stone 0 dec))]
   :east [(fn [stones] (reverse (sort-by first stones))) (fn [stone] (update stone 0 inc))]})

(defn tilt
  ([direction stones]
   (let [[sort-fn update-fn] (directions direction)]
     (tilt sort-fn update-fn stones)))
  ([sort-fn update-fn stones]
   (reduce (fn [stones stone]
             (let [stones (update stones :round set/difference #{stone})
                   new-stone (slide-stone update-fn stone stones)]
               (update stones :round conj new-stone)))
           stones
           (->> stones :round sort-fn))))

(defn stone-load [[_ y] max-y]
  (inc (- max-y y)))

(defn do-p1 [input]
  (let [{:keys [round max-y]} (tilt :north input)]
    (->> round
         (map #(stone-load % max-y))
         (reduce +))))

(defn cycle-stones [stones]
  (->> stones
       (tilt :north)
       (tilt :west)
       (tilt :south)
       (tilt :east)))

(defn find-cycle [stones]
  (loop [stones stones
         seen {(:round stones) 0}
         cnt 0]
    (let [new-stones (cycle-stones stones)]
      (if-let [start (seen (:round new-stones))]
        [start (inc cnt) new-stones]
        (recur
         new-stones
         (assoc seen (:round new-stones) (inc cnt))
         (inc cnt))))))

(defn calculate-load [{:keys [round max-y]}]
  (->> round
       (map #(stone-load % max-y))
       (reduce +)))

(defn do-p2 [stones]
  (let [[cycle-start cycle-end end-state] (find-cycle stones)
        cycle-length (- cycle-end cycle-start)
        remaining (- 1000000000 cycle-start)
        rem (mod remaining cycle-length)]
    (->> end-state
         (iterate cycle-stones)
         (take (inc rem))
         last
         calculate-load)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
