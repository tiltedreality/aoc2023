(ns waltonhoops.aoc2023.p12
  (:require
   [clojure.math.combinatorics :as combo]
   [clojure.string :as string]
   [waltonhoops.aoc2023.utils :as utils]))

(defn parse-line [line]
  (let [[springs groups] (string/split line #" ")
        groups (->> groups (re-seq #"\d+") (mapv parse-long))]
    {:springs (mapv str springs)
     :groups groups}))

(defn parse-input [input]
  (map parse-line input))

(def sample-data
  (->
   "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1"
   string/split-lines
   parse-input))

(def data
  (->> "p12"
       utils/file-lines
       parse-input))

(defn apply-updates [springs updates]
  (reduce #(assoc %1 %2 "#") springs updates))

(defn count-groups [springs]
  (->> springs
       (partition-by identity)
       (filter #(= "#" (first %)))
       (mapv count)))

(defn legal-update? [springs groups updates]
  (let [springs (apply-updates springs updates)]
    (= (count-groups springs) groups)))

(defn row-solutions [{:keys [springs groups]}]
  (let [total-defective (reduce + groups)
        marked-defective (->> springs (filter #(= "#" %)) count)
        remaining-defective (- total-defective marked-defective)
        maybe-defective (->> springs (map-indexed #(when (= "?" %2) %1)) (remove nil?))]
    (->> remaining-defective
         (combo/combinations maybe-defective)
         (filter #(legal-update? springs groups %))
         count)))

(defn do-p1 [input]
  (->> input
       (map row-solutions)
       (reduce +)))

(defn expand-row [{:keys [springs groups]}]
  {:springs (->> springs
                 (repeat 5)
                 (interpose ["?"])
                 (apply concat))
   :groups (->> groups
                (repeat 5)
                (apply concat))})

(def solve-row-internal
  (memoize
   (fn [prev [spring & springs] groups transform]
     (let [spring (cond
                    transform "#"
                    (= spring "?") "."
                    :else spring)
           [group & groups] groups
           group (if (and group (= "#" spring)) (dec group) group)
           groups (if group (cons group groups) nil)
           next-fn (fn [prev springs groups]
                     (+ (solve-row-internal prev springs groups false)
                        (if (= "?" (first springs))
                          (solve-row-internal prev springs groups true)
                          0)))]
       (cond
         (= -1 group) 0

         (and
          (or (nil? group) (zero? group))
          (nil? spring)
          (empty? (rest groups))) 1

         (nil? spring) 0

         (and (= "#" spring) (nil? group)) 0

         (and (= "#" prev) (= "." spring) (zero? group)) (next-fn spring springs (rest groups))

         (and (= "#" prev) (= "." spring)) 0

         :else (next-fn spring springs groups))))))

(defn solve-row [{:keys [springs groups]}]
  (+ (solve-row-internal "." springs groups false)
     (if (= "?" (first springs))
       (solve-row-internal "." springs groups true)
       0)))

(defn do-p2 [input]
  (->> input
       (map expand-row)
       (map solve-row)
       (reduce +)))

(defn p1 []
  (do-p1 data))

(defn p2 []
  (do-p2 data))
